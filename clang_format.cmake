#
# __legal_b__
#
# Copyright (c) 2020 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# __legal_e__
#

include(${CMAKE_CURRENT_LIST_DIR}/dbg_msg.cmake)

option(ESYS_CLANG_FORMAT_MAKE "Run clang-format when building target" Off)

dbg_msg("CMAKE_CURRENT_LIST_DIR = ${CMAKE_CURRENT_LIST_DIR}") 
dbg_msg("CMAKE_CURRENT_SOURCE_DIR = ${CMAKE_CURRENT_SOURCE_DIR}")
dbg_msg("CMAKE_CURRENT_BINARY_DIR = ${CMAKE_CURRENT_BINARY_DIR}")
dbg_msg("CMAKE_FILES_DIRECTORY = ${CMAKE_FILES_DIRECTORY}")
dbg_msg("CMAKE_SOURCE_DIR = ${CMAKE_SOURCE_DIR}")
dbg_msg("CMAKE_BINARY_DIR = ${CMAKE_BINARY_DIR}") 
dbg_msg("PROJECT_BINARY_DIR = ${PROJECT_BINARY_DIR}")
dbg_msg("PROJECT_SOURCE_DIR = ${PROJECT_SOURCE_DIR}")

#
# Detect the presence of clang-format
#
if(NOT CLANG_FORMAT_EXE)
    find_program(CLANG_FORMAT_EXE NAMES clang-format-10 clang-format)
    mark_as_advanced(FORCE CLANG_FORMAT_EXE)
    if(CLANG_FORMAT_EXE)
    message(STATUS "clang-format found: ${CLANG_FORMAT_EXE}")
        else()
    message(STATUS "clang-format not found!")
    endif()
endif()

# Add a new target to format an existing target
#
# Parameter:
# TARGET_NAME - The name of the target for which a formatting target must be created.
#
function(add_clang_format_target TARGET_NAME)
    dbg_msg("add_clang_format_target begin ...")
    if(NOT CLANG_FORMAT_EXE)
        return()
    endif()

    dbg_msg("clang-format found")
    dbg_msg("TARGET_NAME = ${TARGET_NAME}")
    dbg_msg("CMAKE_CURRENT_LIST_DIR = ${CMAKE_CURRENT_LIST_DIR}") 
    dbg_msg("CMAKE_CURRENT_SOURCE_DIR = ${CMAKE_CURRENT_SOURCE_DIR}")
    dbg_msg("CMAKE_CURRENT_BINARY_DIR = ${CMAKE_CURRENT_BINARY_DIR}")
    dbg_msg("CMAKE_FILES_DIRECTORY = ${CMAKE_FILES_DIRECTORY}")
    dbg_msg("CMAKE_SOURCE_DIR = ${CMAKE_SOURCE_DIR}")
    dbg_msg("CMAKE_BINARY_DIR = ${CMAKE_BINARY_DIR}") 
    dbg_msg("PROJECT_BINARY_DIR = ${PROJECT_BINARY_DIR}")
    dbg_msg("PROJECT_SOURCE_DIR = ${PROJECT_SOURCE_DIR}")
    get_target_property(TARGET_NAME_INCLUDES ${TARGET_NAME} INCLUDE_DIRECTORIES)
    dbg_msg("INCLUDES = ${TARGET_NAME_INCLUDES}")

    set(FORMAT_FILES)

    # Check that the TARGET_NAME is indeed a target
    if(NOT TARGET ${TARGET_NAME})
        message(ERROR "${TARGET_NAME} is not a target")
        return()
    endif()

    # The TARGET_NAME is a target, then its source files
    dbg_msg("${TARGET_NAME} is a target")
    get_target_property(_TARGET_TYPE ${TARGET_NAME} TYPE)
    if(NOT _TARGET_TYPE STREQUAL "INTERFACE_LIBRARY")
        get_property(_TEMP TARGET ${TARGET_NAME} PROPERTY SOURCES)
        dbg_msg("_TEMP = ${_TEMP}")
        foreach(iter ${_TEMP})
            if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${iter})
                set(FORMAT_FILES ${FORMAT_FILES} ${CMAKE_CURRENT_SOURCE_DIR}/${iter})
            endif()
        endforeach()
    endif()
    
    dbg_msg("FORMAT_FILES = ${FORMAT_FILES}")

    # If files to format are found, make the new target
    if(FORMAT_FILES)
        set(NEW_TARGET_NAME "format-${TARGET_NAME}")
        if(TARGET ${NEW_TARGET_NAME})
            message(
                ERROR
                "Cannot create clang-format target '${NEW_TARGET_NAME}', already exists.")
        else()
            add_custom_target(${NEW_TARGET_NAME} COMMAND ${CLANG_FORMAT_EXE} -i
                                                -style=file ${FORMAT_FILES})

            if (ESYS_CLANG_FORMAT_MAKE)
                # Add the target calling clang-format target as a dependency
                add_dependencies(${TARGET_NAME} ${NEW_TARGET_NAME})
            endif()

            if(NOT TARGET format)
                add_custom_target(format)
            endif()

            # Add the newly created target as dependency of the target format
            add_dependencies(format ${NEW_TARGET_NAME})
        endif()
    endif()
endfunction()


