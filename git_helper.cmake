#
# __legal_b__
# cmake-format: off
#
# Copyright (c) 2021 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# cmake-format: on
# __legal_e__
#

include(${CMAKE_CURRENT_LIST_DIR}/dbg_msg.cmake)

function(git_find_abs_root SRC_PATH ABS_VAR)
  dbg_msg("git_find_abs_root begin ...")

  execute_process(
    COMMAND git rev-parse --show-toplevel
    WORKING_DIRECTORY "${SRC_PATH}"
    OUTPUT_VARIABLE abs_path_to_repository_root)

  string(STRIP "${abs_path_to_repository_root}" abs_path_to_repository_root)

  set(${ABS_VAR}
      ${abs_path_to_repository_root}
      PARENT_SCOPE)
  dbg_msg("git_find_abs_root end.")
endfunction()
