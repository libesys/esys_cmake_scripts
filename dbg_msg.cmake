#
# __legal_b__
#
# Copyright (c) 2020 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# __legal_e__
#

option(CMAKE_DBG "Print out debug messages" Off)

if(CMAKE_DBG)
macro(DBG_MSG _MSG)
    string(REPLACE "${CMAKE_SOURCE_DIR}/" "" PATH_LOC "${CMAKE_CURRENT_LIST_FILE}")
    message(STATUS "${PATH_LOC}(${CMAKE_CURRENT_LIST_LINE}): ${_MSG}")
endmacro()

macro(DBG_MSG_V _MSG)
  message(STATUS "${CMAKE_CURRENT_LIST_FILE}(${CMAKE_CURRENT_LIST_LINE}): ${_MSG}")
endmacro()

macro(DBG_PRINT_PATH)
    DBG_MSG("PATH = $ENV{PATH}")
endmacro()
else()

function(DBG_MSG _MSG)
endfunction()

function(DBG_MSG_V _MSG)
endfunction()

function(DBG_PRINT_PATH)
endfunction()

endif()