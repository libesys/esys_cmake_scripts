#
# __legal_b__
#
# Copyright (c) 2020 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# __legal_e__
#

include(${CMAKE_CURRENT_LIST_DIR}/dbg_msg.cmake)

if (NOT LSB_RELEASE_EXEC)
find_program(LSB_RELEASE_EXEC lsb_release)

if (LSB_RELEASE_EXEC)
dbg_msg("lsb_release found : ${LSB_RELEASE_EXEC}")
endif()

endif()

function(call_lsb_release PARAM VAR DOC_STR)

if (LSB_RELEASE_EXEC)

if(NOT DEFINED ${VAR})
execute_process(COMMAND ${LSB_RELEASE_EXEC} ${PARAM}
    RESULT_VARIABLE EXEC_RESULT
    OUTPUT_VARIABLE RESULT
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

if (${EXEC_RESULT} EQUAL 0)
set(${VAR} "${RESULT}" PARENT_SCOPE)
message(STATUS "${DOC_STR} : ${RESULT}")
else()
message(ERROR "Calling ${LSB_RELEASE_EXEC} failed. Error = ${EXEC_RESULT}")
endif()

endif()

endif()

endfunction()

call_lsb_release("-is" HOST_OS_NAME "Detected OS")
call_lsb_release("-rs" HOST_OS_REVISION "OS Revision")
call_lsb_release("-cs" HOST_OS_CODENAME "OS Codename")

if(NOT HOST_OS_FULLNAME)
set(HOST_OS_FULLNAME "${HOST_OS_NAME}_${HOST_OS_REVISION}")
message(STATUS "HOST_OS_FULLNAME : ${HOST_OS_FULLNAME}")
endif()
