#
# __legal_b__
# cmake-format: off
#
# Copyright (c) 2020-2021 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# cmake-format: on
# __legal_e__
#

include(${CMAKE_CURRENT_LIST_DIR}/dbg_msg.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/git_helper.cmake)

set(ESYS_CMAKE_SCRIPTS_DIR ${CMAKE_CURRENT_LIST_DIR})
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

#
# Detect the presence of clang-tidy
#
if(NOT CLANG_TIDY_EXE)
  find_program(CLANG_TIDY_EXE NAMES clang-tidy-10 clang-tidy)
  mark_as_advanced(FORCE CLANG_TIDY_EXE)
  if(CLANG_TIDY_EXE)
    message(STATUS "clang-tidy found: ${CLANG_TIDY_EXE}")
  else()
    message(STATUS "clang-tidy not found!")
  endif()
endif()

if(NOT CLANG_TIDY_HTML_EXE)
  find_program(CLANG_TIDY_HTML_EXE NAMES clang-tidy-html)
  mark_as_advanced(FORCE CLANG_TIDY_HTML_EXE)
  if(CLANG_TIDY_HTML_EXE)
    message(STATUS "clang-tidy-html found: ${CLANG_TIDY_HTML_EXE}")
  else()
    message(STATUS "clang-tidy-html not found!")
  endif()
endif()

function(add_clang_tidy_target TARGET_NAME)
  dbg_msg("add_clang_tidy_target begin ...")
  if(NOT CLANG_TIDY_EXE)
    return()
  endif()
  set(one_values BASE_PATH)
  cmake_parse_arguments(PARSE_ARGV 1 add_clang_tidy_target_PREFIX "" "${one_values}"
                        "HEADERS")
  
  set(TIDY_FILES ${add_clang_tidy_target_PREFIX_HEADERS})

  # Check that the TARGET_NAME is indeed a target
  if(NOT TARGET ${TARGET_NAME})
    message(ERROR "${TARGET_NAME} is not a target")
    return()
  endif()

  # The TARGET_NAME is a target, then its source files
  dbg_msg("${TARGET_NAME} is a target")
  get_target_property(_TARGET_TYPE ${TARGET_NAME} TYPE)
  if(NOT _TARGET_TYPE STREQUAL "INTERFACE_LIBRARY")
    get_property(
      _TEMP
      TARGET ${TARGET_NAME}
      PROPERTY SOURCES)
    dbg_msg("_TEMP = ${_TEMP}")
    foreach(iter ${_TEMP})
      if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${iter})
        set(TIDY_FILES ${TIDY_FILES} ${CMAKE_CURRENT_SOURCE_DIR}/${iter})
      endif()
    endforeach()
  endif()

  dbg_msg("TIDY_FILES = ${TIDY_FILES}")

  # If files to format are found, make the new target
  if(TIDY_FILES)
    set(ALT_NEW_TARGET_NAME "tidy-${TARGET_NAME}")
    set(ALT_FIX_TARGET_NAME "tidy-fix-${TARGET_NAME}")
    set(NEW_TARGET_NAME "${TARGET_NAME}-tidy")
    set(FIX_TARGET_NAME "${TARGET_NAME}-tidy-fix")

    set(FIXED_TIDY_FILES)
    foreach(iter ${TIDY_FILES})
      if(${CMAKE_VERSION} VERSION_GREATER_EQUAL "3.20")
        set(the_path ${iter})
        cmake_path(NORMAL_PATH iter OUTPUT_VARIABLE the_file)
      else()
        get_filename_component(the_file ${iter} REALPATH)
      endif()  
      set(FIXED_TIDY_FILES ${FIXED_TIDY_FILES} ${the_file})
    endforeach()
    
    if(TARGET ${NEW_TARGET_NAME})
      message(
        ERROR
        "Cannot create clang-tidy target '${NEW_TARGET_NAME}', already exists.")
    else()
      configure_file(${ESYS_CMAKE_SCRIPTS_DIR}/clang-tidy-sed.sh.in
                 ${CMAKE_CURRENT_BINARY_DIR}/clang-tidy-sed.sh)
      add_custom_target(
        ${NEW_TARGET_NAME}
        COMMAND
          ${ESYS_CMAKE_SCRIPTS_DIR}/clang_tidy_target.py
          --clang-tidy=${CLANG_TIDY_EXE}
          --json-file=${CMAKE_BINARY_DIR}/compile_commands.json
          --target=${TARGET_NAME}
          --json-output=${CMAKE_BINARY_DIR}/${TARGET_NAME}-compile_commands.json
          ${FIXED_TIDY_FILES} | tee ${TARGET_NAME}-clang_tidy.log
        COMMAND ${CMAKE_CURRENT_BINARY_DIR}/clang-tidy-sed.sh USES_TERMINAL
        COMMAND echo "clang-tidy report ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-clang_tidy.log")
      add_custom_target(
        ${FIX_TARGET_NAME}
        COMMAND
          ${ESYS_CMAKE_SCRIPTS_DIR}/clang_tidy_target.py
          --clang-tidy=${CLANG_TIDY_EXE}
          --json-file=${CMAKE_BINARY_DIR}/compile_commands.json
          --target=${TARGET_NAME}
          --json-output=${CMAKE_BINARY_DIR}/${TARGET_NAME}-compile_commands.json
          ${FIXED_TIDY_FILES} --fix)

      add_custom_target(
        ${ALT_NEW_TARGET_NAME}
        COMMAND echo
                "${ALT_NEW_TARGET_NAME} is deprecated, use ${NEW_TARGET_NAME}"
        DEPENDS ${NEW_TARGET_NAME})

      add_custom_target(
        ${ALT_FIX_TARGET_NAME}
        COMMAND echo
                "${ALT_FIX_TARGET_NAME} is deprecated, use ${FIX_TARGET_NAME}"
        DEPENDS ${FIX_TARGET_NAME})

      if(CLANG_TIDY_HTML_EXE)
        add_custom_target(
          ${NEW_TARGET_NAME}-html
          COMMAND ${CLANG_TIDY_HTML_EXE} ${TARGET_NAME}-clang_tidy.log
          COMMAND cp clang.html ${TARGET_NAME}-clang_tidy.html
          COMMAND ${CMAKE_CURRENT_BINARY_DIR}/clang-tidy-sed.sh USES_TERMINAL
          COMMAND echo "clang_tidy HTML report ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-clang_tidy.html"
          DEPENDS ${NEW_TARGET_NAME})

        add_custom_target(
          ${NEW_TARGET_NAME}-html-only
          COMMAND ${CLANG_TIDY_HTML_EXE} ${TARGET_NAME}-clang_tidy.log
          COMMAND cp clang.html ${TARGET_NAME}-clang_tidy.html
          COMMAND ${CMAKE_CURRENT_BINARY_DIR}/clang-tidy-sed.sh USES_TERMINAL
          COMMAND echo "clang-tidy HTML report ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-clang_tidy.html"
          )
      endif()

      if(NOT TARGET tidy)
        add_custom_target(tidy)
      endif()

      # Add the newly created target as dependency of the target tidy
      add_dependencies(tidy ${NEW_TARGET_NAME})
    endif()
  endif(TIDY_FILES)

  set(NEW_TARGET_DIFF_NAME "${TARGET_NAME}-tidy-diff")
  get_property(
    THE_SOURCE_PATH
    TARGET ${TARGET_NAME}
    PROPERTY SOURCE_DIR)
  git_find_abs_root(${THE_SOURCE_PATH} GIT_ABS_ROOT)
  configure_file(${ESYS_CMAKE_SCRIPTS_DIR}/clang-tidy-diff.sh.in
                 ${CMAKE_CURRENT_BINARY_DIR}/clang-tidy-diff.sh)
  add_custom_target(
    ${NEW_TARGET_DIFF_NAME}
    ${CMAKE_CURRENT_BINARY_DIR}/clang-tidy-diff.sh
    USES_TERMINAL)
  # ${CMAKE_BINARY_DIR}/compile_commands.json
endfunction()
