#
# __legal_b__
# cmake-format: off
#
# Copyright (c) 2021 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# cmake-format: on
# __legal_e__
#

include(${CMAKE_CURRENT_LIST_DIR}/dbg_msg.cmake)

set(ESYS_CMAKE_SCRIPTS_DIR ${CMAKE_CURRENT_LIST_DIR})

find_program(VALGRIND_EXE NAMES valgrind)
mark_as_advanced(FORCE VALGRIND_EXE)
if(VALGRIND_EXE)
  message(STATUS "valgrind found: ${VALGRIND_EXE}")
else()
  message(STATUS "valgrind not found!")
endif()

find_program(VALGRIND_CI_EXE NAMES valgrind-ci)
mark_as_advanced(FORCE VALGRIND_CI_EXE)
if(VALGRIND_CI_EXE)
  message(STATUS "valgrind-ci found: ${VALGRIND_CI_EXE}")
else()
  message(STATUS "valgrind-ci not found!")
endif()

function(add_valgrind_target TARGET_NAME)
  dbg_msg("add_valgrind_target begin ...")
  if(NOT VALGRIND_EXE)
    return()
  endif()
  if(NOT DEFINED VALGRIND_LINES_BEFORE)
    set(VALGRIND_LINES_BEFORE 5)
  endif()
  if(NOT DEFINED VALGRIND_LINES_AFTER)
    set(VALGRIND_LINES_AFTER 5)
  endif()
  add_custom_target(
    ${TARGET_NAME}-valgrind
    COMMAND
      ${VALGRIND_EXE} --leak-check=full --show-leak-kinds=all
      --track-origins=yes --verbose --log-file=${TARGET_NAME}-valgrind-out.txt
      --xml=yes --xml-file=${TARGET_NAME}-valgrind-out.xml
      $<TARGET_FILE:${TARGET_NAME}>
      --logger=HRF,test_suite,stdout:JUNIT,all,${TARGET_NAME}-report_junit.txt
    COMMAND echo "valgrind reports:"
    COMMAND echo "    log   ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-valgrind-out.txt"
    COMMAND echo "    xml   ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-valgrind-out.xml"
    COMMAND echo "    junit ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-report_junit.txt"
    DEPENDS ${TARGET_NAME})
  if(NOT VALGRIND_CI_EXE)
    return()
  endif()
  get_target_property(TARGET_SOURCE_DIR ${TARGET_NAME} SOURCE_DIR)
  get_target_property(TARGET_BINARY_DIR ${TARGET_NAME} BINARY_DIR)
  add_custom_target(
    ${TARGET_NAME}-valgrind-html-only
    COMMAND
      ${VALGRIND_CI_EXE} ${TARGET_NAME}-valgrind-out.xml
      --source-dir=${CMAKE_SOURCE_DIR}
      --output-dir=${TARGET_BINARY_DIR}/valgrind_html
      --lines-before=${VALGRIND_LINES_BEFORE}
      --lines-after=${VALGRIND_LINES_AFTER}
      COMMAND echo "valgrind HTML report ${TARGET_BINARY_DIR}/valgrind_html/index.html"
      )
  add_custom_target(
    ${TARGET_NAME}-valgrind-html
    COMMAND
      ${VALGRIND_CI_EXE} ${TARGET_NAME}-valgrind-out.xml
      --source-dir=${CMAKE_SOURCE_DIR}
      --output-dir=${TARGET_BINARY_DIR}/valgrind_html
      --lines-before=${VALGRIND_LINES_BEFORE}
      --lines-after=${VALGRIND_LINES_AFTER}
      COMMAND echo "valgrind HTML report ${TARGET_BINARY_DIR}/valgrind_html/index.html"
    DEPENDS ${TARGET_NAME}-valgrind)

  dbg_msg("add_valgrind_target end.")
endfunction()
