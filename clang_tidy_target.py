#!/usr/bin/env python3

#
# __legal_b__
#
# Copyright (c) 2020-2021 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
#
# __legal_e__
#

import os
import sys
import argparse
import json
import subprocess

class CLangTidyTarget:
    def __init__(self):
        self.m_exe = None
        self.m_json_file = None
        self.m_files = None
        self.m_target_name = None
        self.m_json_output = None
        self.m_fix = None

    def set_exe(self, exe):
        self.m_exe = exe

    def get_exe(self):
        return self.m_exe

    def set_json_file(self, json_file):
        self.m_json_file = json_file

    def get_json_file(self):
        return self.m_json_file

    def set_files(self, files):
        self.m_files = files

    def get_files(self):
        return self.m_files

    def set_target_name(self, target_name):
        self.m_target_name = target_name

    def get_target_name(self):
        return self.m_target_name

    def set_json_output(self, json_output):
        self.m_json_output = json_output

    def get_json_output(self):
        return self.m_json_output

    def set_fix(self, fix):
        self.m_fix = fix

    def get_fix(self):
        return self.m_fix

    def run(self):
        self.create_target_json_file()
        self.call_clang_tidy()

    def create_target_json_file(self):
        input_data = None
        with open(self.get_json_file(), "r") as read_it: 
            input_data = json.load(read_it) 

        output_data = []
        
        for item in input_data:
            if item["file"] in self.get_files():
                output_data.append(item)
        
        with open(self.get_json_output(), "w") as outf: 
            json.dump(output_data, outf, indent=2) 

    def call_clang_tidy(self):
        command = []
        command.append(self.get_exe())
        command.append("-p="+self.get_json_output())
        command.append("-header-filter='.*'")
        if self.get_fix():
            command.append("-fix")
            
        for input_file in self.get_files():
            command.append(input_file)
    
        result = subprocess.run(command)
        if result.returncode != 0:
            print("ERROR running cland-tidy: %s" % result.returncode)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("files", default=None, nargs='*', help="input file(s)" )
    parser.add_argument("--clang-tidy", dest="clang_tidy", default=None, help="the path to clang-tidy")
    parser.add_argument("--json-file", dest="json_file", default=None, help="the CMake compiler json")
    parser.add_argument("--target", dest="target", default=None, help="the name of the target")
    parser.add_argument("--json-output", dest="json_output", default=None, help="json output for the target")
    parser.add_argument("--fix", dest="fix", action="store_true", default=False, help="let clang-tidy fix the issues")

    args = parser.parse_args()

    obj = CLangTidyTarget()
    obj.set_exe(args.clang_tidy)
    obj.set_json_file(args.json_file)
    obj.set_files(args.files)
    obj.set_target_name(args.target)
    obj.set_json_output(args.json_output)
    obj.set_fix(args.fix)
    
    obj.run()

if __name__ == "__main__":
    main()